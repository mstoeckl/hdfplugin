/*
 * Copyright (c) 2018 ???
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES ( INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION ) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ( INCLUDING NEGLIGENCE OR OTHERWISE ) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hdfplugin.hh"
#include "ark_debug.h"
#include "queries.h"

#include <KIO/Global>
#include <KLocalizedString>
#include <KPluginFactory>

#include <QDataStream>
#include <QDateTime>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <qplatformdefs.h>
#include <QThread>
#include <QProcess>
#include <QTemporaryFile>

K_PLUGIN_FACTORY_WITH_JSON(HDFPluginFactory, "kerfuffle_hdf.json", registerPlugin<HDFPlugin>();)

struct attr_backref {
      QFile* file;
      HDFPlugin* plugin;
};

herr_t HDFPlugin::iterLink( hid_t g_id, const char *name, const H5L_info_t *info, void *vplugin)  {
    HDFPlugin* plugin = (  HDFPlugin*)vplugin;
    plugin->listEntry(g_id, name, info);
    return 0; // 0 means continue
}
herr_t HDFPlugin::iterAttr(hid_t location_id, const char *attr_name, const H5A_info_t *ainfo, void *op_data) {
    struct attr_backref* s = (struct attr_backref*)op_data;
    s->plugin->printAttr(*s->file, location_id, attr_name, ainfo);
    return 0;
}

enum {
    fileIsHDF4,
    fileIsHDF5,
    fileIsNeither
};

static int identifyFile(const QString& name, qint64* user_block_size) {
    const unsigned char bmagic4[5] = { 0x0e, 0x03, 0x13, 0x01, 0};
    const unsigned char bmagic5[9] = { 0x89,0x48,0x44,0x46,0x0d,0x0a,0x1a,0x0a, 0};
    QByteArray magic4((const char*)bmagic4, 4);
    QByteArray magic5((const char*)bmagic5, 8);

    bool is_hdf4 = false;
    QFile fprobe(name);
    bool success = fprobe.open(QFile::ReadOnly);
    if (!success) {
        qDebug("Failed to open file");
        return fileIsNeither;
    }
    qint64 sz = fprobe.size();
    QByteArray head = fprobe.read(8);
    if (head.startsWith(magic4)) {
        // Thanks to the user block, we could have simultaneous HDF4 and HDF5
        // files!) Correspondingly, we should always fully test for HDF5 before
        // going for HDF4, as the HDF4 can then be extracted with only one more click
        is_hdf4 = true;
    }
    if (head.startsWith(magic5)) {
        *user_block_size = 0;
        return fileIsHDF5;
    }
    qint64 offset = 512;
    while (offset < sz) {
        if (!fprobe.seek(offset)) {
            qDebug("File does not support random access");
            return fileIsNeither;
        }
        QByteArray head = fprobe.read(8);
        if (head.startsWith(magic5)) {
            *user_block_size = offset;
            return fileIsHDF5;
        }
        offset*= 2;
    }

    if (is_hdf4) {
        return fileIsHDF4;
    }
    return fileIsNeither;
}


HDFPlugin::HDFPlugin(QObject *parent, const QVariantList & args)
    : ReadOnlyArchiveInterface(parent, args)
    , m_overwriteAll(false)
    , m_skipAll(false)
    , m_listAfterAdd(false)
    , file_id(0)
    , user_block_size(0)
{
    qCDebug(ARK) << "Initializing HDF plugin";

    QString fname = filename();
    int cat = identifyFile(fname, &user_block_size);

    // If the file is HDF5, file. If HDF4, convert to HDF5, open, and (implicitly)
    // unlink the temporary file
    QTemporaryFile tfile;
    switch (cat) {
    default:
    case fileIsNeither:
        qDebug("Misidentified file");
        return;
    case fileIsHDF4:
        qDebug("HDF4 file, so we run h4toh5 & change filename");
        {
            int32 h4id = Hopen(fname.toUtf8().constData(), DFACC_READ, 0);

            uint32 major_v, minor_v, release;
            char string[256];
            intn status = Hgetfileversion(h4id, &major_v, &minor_v, &release, string);
            if (status < 0) {
                qDebug("Failed to open file");
            }
            qDebug("%d | %u %u %u %s\n", status, major_v, minor_v, release, string);

            // dimensions; variables; attributes; data

            Hclose(h4id);

            if (!tfile.open()) {
                qDebug("Failed to open temp file for h4toh5");
                return;
            }
            fname = tfile.fileName();
            int retcode = QProcess::execute(QStringLiteral("h4toh5"), QStringList()<<filename()<<fname);
            if (retcode != 0) {
                qDebug("Failed to convert '%s' with h4toh5", filename().toUtf8().constData());
                return;
            }
            // successfully converted
            cat = fileIsHDF5;
        }
        break;
    case fileIsHDF5:
        qDebug("HDF5 file, user block size is %lld", user_block_size);
        break;
    }

    file_id = H5Fopen(fname.toUtf8().constData(), H5F_ACC_RDONLY, H5P_DEFAULT);
    if (file_id < 0) {
        // Failed to load; must not actually be an HDF5 file.
        return;
    }
}

HDFPlugin::~HDFPlugin()
{
    if (file_id > 0) {
        H5Fclose(file_id);
    }

    foreach (const auto e, m_emittedEntries) {
        // Entries might be passed to pending slots, so we just schedule their deletion.
        e->deleteLater();
    }
}

static QString get_parent_path(QString path) {
    QStringList sl = path.split(QChar::fromLatin1('/'));
    if (sl.size() > 1) {
        sl.pop_back();
        return sl.join(QChar::fromLatin1('/'));
    } else {
        return QString();
    }
}

// it may be that info is null
void HDFPlugin::listEntry(hid_t root_id, const char *name, const H5L_info_t */*info*/) {
    QString path(QString::fromUtf8(name));
    QString parent = get_parent_path(path);

    Archive::Entry* ent = new Archive::Entry();
    connect(this, &QObject::destroyed, ent, &QObject::deleteLater);
    ent->setFullPath(path);
    if (!parent.isEmpty()) {
        ent->setParent(m_emittedEntries[parent]);
    }
    m_emittedEntries[path] = ent;
    ent->setIsDirectory(true);

    hid_t obj_id = H5Oopen(root_id, name, H5P_DEFAULT);

    H5O_info_t oi;
    H5Oget_info(obj_id, &oi);
    // Timestamp for the group is ctime. (For dataset data, use mtime)
    ent->setProperty("timestamp", QDateTime::fromSecsSinceEpoch(oi.ctime));

    emit entry(ent);

    // Add attributes (as any object can have them)
    listAttr(obj_id, path);

    switch (oi.type) {
    default:
    case H5O_TYPE_UNKNOWN: {
        // create an empty node with unknown child
        Archive::Entry* sentinel = new Archive::Entry();
        sentinel->setFullPath(path+QStringLiteral("/unknown"));
        m_emittedEntries[sentinel->fullPath()] = sentinel;
        emit entry(sentinel);
    }   break;
    case H5O_TYPE_GROUP: {
        // Equivalent to a folder; already exists, so do nothing

    }   break;
    case H5O_TYPE_DATASET: {
        // Has two children (attr, data)
        Archive::Entry* data = new Archive::Entry();
        data->setFullPath(path+QStringLiteral("/data"));
        m_emittedEntries[data->fullPath()] = data;

        Archive::Entry* type = new Archive::Entry();
        type->setFullPath(path+QStringLiteral("/type"));
        m_emittedEntries[type->fullPath()] = type;
        type->setProperty("size", 200);//placeholder

        // Q: can a group have attributes?

        hid_t type_id = H5Dget_type(obj_id);
        hid_t space = H5Dget_space(obj_id);
        hssize_t npts = H5Sget_simple_extent_npoints(space);
        size_t ts = H5Tget_size(type_id);
        qint64 size = ts * npts;
        data->setProperty("size", size);

        // ignores metadata overhead
        hsize_t ds_storage = H5Dget_storage_size(obj_id);
        data->setProperty("compressedSize", ds_storage);

        ent->appendEntry(data);
        ent->appendEntry(type);
        emit entry(data);
        emit entry(type);

    }   break;
    case H5O_TYPE_NAMED_DATATYPE: {
        Archive::Entry* sentinel = new Archive::Entry();
        sentinel->setFullPath(path+QStringLiteral("/named_datatype"));
        m_emittedEntries[sentinel->fullPath()] = sentinel;
        emit entry(sentinel);
    }    break;
    }


    H5Oclose(obj_id);
}

void HDFPlugin::listAttr(hid_t obj_id, const QString& obj_path) {
    int natter = H5Aget_num_attrs(obj_id);
    if (natter > 0) {
        Archive::Entry* attr = new Archive::Entry();
        attr->setFullPath(obj_path+QStringLiteral("/attr"));
        m_emittedEntries[attr->fullPath()] = attr;
        attr->setProperty("size", natter * 100);
        emit entry(attr);
    }
}

bool HDFPlugin::list()
{

    qCDebug(ARK) << "Listing archive contents for:" << QFile::encodeName(filename());

    // NOTE: it's possible that legitimate HDF5 files get miscategorized
    // and fail to open because Ark prefers content-based mimetypes, so
    // when the user block has a format that QMimeDatabase recognizes and
    // marks as 'isReadable', this plugin will not be called up.



    // Navigation goes by groups and links.

    // TODO: make the `root` group an explicit entry, with the same
    // name as the file (but with ending stripped).

    H5O_info_t oi;
    herr_t status = H5Oget_info_by_name(file_id, "/", &oi, H5P_DEFAULT);
    qDebug("got root element %d, time %ld status %d, attrs %llu", oi.type, (long)oi.ctime, status, oi.num_attrs);
    hid_t root = H5Oopen(file_id, "/", H5P_DEFAULT);
    H5Lvisit(root, H5_INDEX_NAME, H5_ITER_INC, &HDFPlugin::iterLink, this);

    listAttr(root, QString());

    if (user_block_size > 0) {
        Archive::Entry* attr = new Archive::Entry();
        attr->setFullPath(QStringLiteral("/userblock"));
        m_emittedEntries[attr->fullPath()] = attr;
        attr->setProperty("size", user_block_size);
        emit entry(attr);
    }

//    hsize_t usrblsz;
//    if (H5Pget_userblock())

    H5Oclose(root);
    // Q: when to close?

    m_listAfterAdd = false;
    return true;
}

bool HDFPlugin::testArchive()
{
    qCDebug(ARK) << "Testing archive";

    // See also `h5check`

//        emit progress(float(i) / nofEntries);
//    emit testSuccess();
//    return true;
    return true;
}

bool HDFPlugin::doKill()
{
    QMutexLocker mutexLocker(&m_mutex);

    return true;
}

static QString string_c_escape(QString alt) {
    // map " to \", \ to \\, newline to \n
    alt = alt.replace(QStringLiteral("\\"),QStringLiteral("\\\\"));
    alt = alt.replace(QStringLiteral("\n"),QStringLiteral("\\\n"));
    alt = alt.replace(QStringLiteral("\""),QStringLiteral("\\\""));
    return QStringLiteral("\"")+alt+QStringLiteral("\"");
}

static QString format_dataspace(hid_t space) {
    QStringList properties;

    H5S_class_t cls = H5Sget_simple_extent_type(space);
    switch (cls) {
    default:
    case H5S_NO_CLASS:
        properties << QStringLiteral("ERROR");
        break;
    case H5S_NULL:
        properties << QStringLiteral("NULL");
        break;
    case H5S_SCALAR:
        properties << QStringLiteral("SCALAR");
        break;
    case H5S_SIMPLE:
        properties << QStringLiteral("SIMPLE");
        break;
    }
    if (cls == H5S_SIMPLE) {
        int ndims = H5Sget_simple_extent_ndims(space);
        if (ndims >= 0) {
            hsize_t* dims = new hsize_t[ndims];
            hsize_t* maxdims = new hsize_t[ndims];
            herr_t err = H5Sget_simple_extent_dims(space, dims, maxdims);
            if (err < 0) {
                properties << QString(QStringLiteral("NDIMS=%1")).arg(ndims);
                properties << QStringLiteral("<invalid dimensions>");
            } else {
                QStringList ldims, lmax;
                for (int i=0;i<ndims;i++) {
                    if (dims[i] == H5S_UNLIMITED) {
                        ldims << QStringLiteral("UNLIMITED");
                    } else {
                        ldims << QString(QStringLiteral("%1")).arg(dims[i]);
                    }
                    if (maxdims[i] == H5S_UNLIMITED) {
                        lmax << QStringLiteral("UNLIMITED");
                    } else {
                        lmax << QString(QStringLiteral("%1")).arg(dims[i]);
                    }
                }
                properties << QStringLiteral("DIMS=[")+ldims.join(QStringLiteral(", "))+QStringLiteral("]");
                properties << QStringLiteral("MAXDIMS=[")+lmax.join(QStringLiteral(", "))+QStringLiteral("]");
            }
            delete [] dims;
            delete [] maxdims;
        } else {
            properties << QStringLiteral("<invalid simple dimension>");
        }
    }

    return properties.join(QChar::fromLatin1('\t'));
}

static QString format_byte_order(H5T_order_t order) {
    switch (order) {
    default:
    case H5T_ORDER_ERROR:
        return QStringLiteral("ORDER_ERROR");
    case H5T_ORDER_LE:
        return QStringLiteral("ORDER_LE");
    case H5T_ORDER_BE:
        return QStringLiteral("ORDER_BE");
    case H5T_ORDER_VAX:
        return QStringLiteral("ORDER_VAX");
    case H5T_ORDER_MIXED:
        return QStringLiteral("ORDER_MIXED");
    case H5T_ORDER_NONE:
        return QStringLiteral("ORDER_NONE");
    }
}

static QString format_datatype(hid_t type) {
    QStringList properties;
    switch (H5Tget_class(type)) {
    case H5T_STRING: {
        properties << QStringLiteral("STRING");
        H5T_cset_t cset = H5Tget_cset(type);
        switch (cset) {
        case H5T_CSET_UTF8:
            properties << QStringLiteral("CSET_UTF8");
            break;
        case H5T_CSET_ASCII:
            properties << QStringLiteral("CSET_ASCII");
            break;
        case H5T_CSET_ERROR:
            properties << QStringLiteral("CSET_ERROR");
            break;
        default:
            properties << QString(QStringLiteral("CSET_RESERVED_%1")).arg((int)cset);
            break;
        }
        H5T_str_t pad = H5Tget_strpad(type);
        switch (pad) {
        case H5T_STR_ERROR:
            properties << QStringLiteral("STR_ERROR");
            break;
        case H5T_STR_NULLTERM:
            properties << QStringLiteral("STR_NULLTERM");
            break;
        case H5T_STR_NULLPAD:
            properties << QStringLiteral("STR_NULLPAD");
            break;
        case H5T_STR_SPACEPAD:
            properties << QStringLiteral("STR_SPACEPAD");
            break;
        default:
            properties << QString(QStringLiteral("STR_RESERVED_%1")).arg((int)cset);
            break;
        }
        htri_t is_vlstr = H5Tis_variable_str(type);
        if (is_vlstr) {
            properties << QStringLiteral("VARIABLE");
        } else {
            properties << QString(QStringLiteral("LENGTH=%1")).arg(H5Tget_size(type));
        }
    }   break;
    case H5T_INTEGER: {
        properties << QStringLiteral("INTEGER");
        properties << format_byte_order(H5Tget_order(type));
        switch (H5Tget_sign(type)) {
        default:
        case H5T_SGN_ERROR:
            properties << QStringLiteral("SGN_ERROR");
            break;
        case H5T_SGN_NONE:
            properties << QStringLiteral("SGN_UNSIGNED");
            break;
        case H5T_SGN_2:
            properties << QStringLiteral("SGN_SIGNED");
            break;
        }
        properties << QStringLiteral("PRECISION=%1").arg(H5Tget_precision(type));

        // padding/offset information?
    }   break;
    case H5T_FLOAT: {
        properties << QStringLiteral("FLOAT");
        properties << format_byte_order(H5Tget_order(type));

        /* are pointers to locations */
        // size_t spos, epos, esize, mpos, msize;
        // herr_t err = H5Tget_fields(type, &spos, &epos, &esize,&mpos,&msize);

        size_t prec = H5Tget_precision(type);
        if (prec == 32) {
            properties << QStringLiteral("SINGLE");
        } else if (prec == 64) {
            properties << QStringLiteral("DOUBLE");
         } else {
            properties << QStringLiteral("BITS=%1").arg(prec);

        }
    }   break;
    default:
        properties << QStringLiteral("<untranslatable datatype>");

    }



    return properties.join(QChar::fromLatin1('\t'));
}

static void format_data(hid_t aid, QStringList& list) {
    hid_t type = H5Aget_type(aid);
    hid_t space = H5Aget_space(aid);
    hssize_t npts = H5Sget_simple_extent_npoints(space);
    if (npts < 1) {
        list << QStringLiteral("<empty>");
        return;
    }

//    qDebug("arr pts %lld", npts);

    switch (H5Tget_class(type)) {
    case H5T_STRING: {
        H5T_cset_t cset = H5Tget_cset(type);
        if (cset != H5T_CSET_UTF8 && cset != H5T_CSET_ASCII) {
            list << QStringLiteral("<untranslatable string type>");
            return;
        }

        H5T_str_t pad = H5Tget_strpad(type);
        (void)pad; // typically: space pad, null pad, null term
        htri_t is_vlstr = H5Tis_variable_str(type);

        size_t ts = H5Tget_size(type);
        char* buf = (char*)calloc(npts,ts);
        herr_t status = H5Aread(aid, type, buf);
        if (status < 0) {
            list << QStringLiteral("<unreadable string type>");
            return;
        }

        for (int i=0;i<npts;i++) {
            char* seg = &buf[i*ts];
            if (is_vlstr) {
                char** ptr = (char**)seg;
                list << string_c_escape(QString::fromUtf8(*ptr));
            } else {
                int len = (int)strnlen(seg, (size_t)ts);
                list << string_c_escape(QString::fromUtf8(seg, len));
            }
        }

        free(buf);
        return;
    }
    case H5T_BITFIELD:
    case H5T_INTEGER:
    case H5T_FLOAT: {
        // Smallest float type which is convertible.
        hid_t native_type = H5Tget_native_type(type, H5T_DIR_ASCEND);
        size_t ts = H5Tget_size(native_type);
        char* buf = (char*)calloc(npts,ts);
        herr_t status =  H5Aread(aid, native_type, buf);
        char ret[256];
        if (status < 0) {
            list << QLatin1String("<untranslatable number type>");
        } else if (H5Tequal(native_type,H5T_NATIVE_FLOAT)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%.9g", *(float*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_DOUBLE)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%.17g", *(double*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_LDOUBLE)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%.21Lg", *(long double*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_CHAR)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%d", *(char*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_SHORT)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%d", *(short*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_INT)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%d", *(int*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_LONG)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%ld", *(long*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_LLONG)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%lld", *(long long*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_UCHAR)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%u", *(unsigned char*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_USHORT)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%u", *(unsigned short*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_UINT)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%u", *(unsigned int*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_ULONG)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%lu", *(unsigned long*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_ULLONG)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%llu", *(unsigned long long*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_B8)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%x", *(uint8_t*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_B16)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%x", *(uint16_t*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_B32)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%x", *(uint32_t*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else if (H5Tequal(native_type,H5T_NATIVE_B64)) {
            for (int i=0;i<npts;i++) {
                sprintf(ret, "%lx", *(uint64_t*)&buf[i*ts]);
                list << QLatin1String(ret);
            }
        } else {
            list << QLatin1String("<untranslatable number type>");
        }

        free(buf);
        H5Tclose(native_type);
        return;
    }
    case H5T_NO_CLASS:
        list << QStringLiteral("<untranslatable error type>");
        return;
    case H5T_TIME:
        list << QStringLiteral("<untranslatable time type>");
        return;
    case H5T_OPAQUE:
        list << QStringLiteral("<untranslatable opaque type>");
        return;
    case H5T_COMPOUND:
        list << QStringLiteral("<untranslatable compound type>");
        return;
    case H5T_REFERENCE:
        list << QStringLiteral("<untranslatable reference type>");
        return;
    case H5T_ENUM: {
        size_t ts = H5Tget_size(H5T_NATIVE_INT);
        char* buf = (char*)calloc(npts,ts);
        herr_t status =  H5Aread(aid, H5T_NATIVE_UINT, buf);
        if (status < 0) {
            list << QStringLiteral("<untranslatable enum type>");
        } else {
            for (int i=0;i<npts;i++) {
                uint v = *(uint*)&buf[i*ts];
                char* name = H5Tget_member_name(type, v);
                if (name) {
                    list << QString::fromUtf8(name);
                    H5free_memory(name);
                } else {
                    list << QStringLiteral("<unavailable code>");
                }
            }
        }
    }   return;
    case H5T_VLEN:
        list << QStringLiteral("<untranslatable variable length type>");
        return;
    case H5T_ARRAY:
        list << QStringLiteral("<untranslatable array type>");
        return;
    default:
        list << QStringLiteral("<untranslatable unknown type>");
        return;
    }
}

void HDFPlugin::printAttr(QFile& dest, hid_t loc_id, const char* name, const H5A_info_t* /*info*/) {
    QString text = QLatin1String(name) + QLatin1String(": ");
    dest.write(text.toUtf8().constData());

    hid_t aid = H5Aopen_by_name(loc_id, ".", name, H5P_DEFAULT, H5P_DEFAULT);

    QStringList datalist;
    format_data(aid, datalist);
    QString data = datalist.join(QChar::fromLatin1('\t'));
    dest.write(data.toUtf8().constData());
    dest.write("\n");

    H5Aclose(aid);

}

bool HDFPlugin::extractEntry(hid_t root_id, const Archive::Entry* entry, const QString& destDir, const ExtractionOptions& options) {
    const bool removeRootNode = options.isDragAndDropEnabled();

    QString destDirCorrected(destDir);
    if (!destDir.endsWith(QDir::separator())) {
        destDirCorrected.append(QDir::separator());
    }

    // TODO: use QDir to avoid path elevation security risks
    // (actually, use QDir for all paths)
    QString destination = destDirCorrected + entry->fullPath();
    if (entry->isDir()) {
        // Create directory if it doesn't already exist
        qDebug("extract folder '%s'", destination.toUtf8().constData());
        QDir().mkpath(destination);
    } else {
        QDir().mkpath(QFileInfo(destination).path());
        qDebug("extract file '%s'", destination.toUtf8().constData());


        QFile dest(destination);
        dest.open(QFile::WriteOnly);

        // Open object, unless root, in which case already open
        QString pp = get_parent_path(entry->fullPath());
        bool is_root = pp.isEmpty();
        const char* name = pp.toUtf8().constData();
        hid_t dset_id = is_root ? root_id : H5Oopen(root_id, name, H5P_DEFAULT);
        if (dset_id < 0) {
            qWarning("Could not locate dataset '%s'", name);
        }

        if (entry->name() == QLatin1String("data")) {
            // remove the '/data' suffix
            hid_t space = H5Dget_space(dset_id);
            hid_t type = H5Dget_type(dset_id);
            hssize_t npts = H5Sget_simple_extent_npoints(space);
            size_t ts = H5Tget_size(type);

            char* buf = (char*)calloc(ts, npts);
            H5Dread(dset_id, type, H5S_ALL,H5S_ALL,H5P_DEFAULT, buf);
            dest.write(buf, ts*npts);
            free(buf);

        } else if (entry->name() == QLatin1String("type")) {
            hid_t space = H5Dget_space(dset_id);
            hid_t type = H5Dget_type(dset_id);

            dest.write("Type: ");
            dest.write(format_datatype(type).toUtf8().constData());
            dest.write("\n");

            dest.write("Space: ");
            dest.write(format_dataspace(space).toUtf8().constData());
            dest.write("\n");

        } else if  (entry->name()  == QLatin1String("attr")) {
            struct attr_backref back;
            back.file = &dest;
            back.plugin = this;
            H5Aiterate2(dset_id, H5_INDEX_NAME, H5_ITER_INC, NULL, &HDFPlugin::iterAttr, &back);
        } else if (entry->name() == QLatin1String("unknown")) {

        } else if(entry->name() == QLatin1String("named_datatype")) {

        } else if(entry->name() == QLatin1String("userblock")) {
            QFile refile(filename());
            if (!refile.open(QFile::ReadOnly)) {
                qDebug("Failed to reopen file '%s'", filename().toUtf8().constData());
            }
            qint64 size = entry->property("size").toLongLong();
            dest.write(refile.read(size));
        } else {
            qDebug("Bad write case for '%s'",entry->name().toUtf8().constData());
        }

        if (!is_root) {
           H5Oclose(dset_id);
        }
    }
    return true; // successful
}

bool HDFPlugin::extractFiles(const QVector<Archive::Entry*> &files, const QString& destinationDirectory, const ExtractionOptions& options)
{
    qCDebug(ARK) << "Extracting files to:" << destinationDirectory;

    const bool extractAll = files.isEmpty();
    const QVector<Archive::Entry*> flist = extractAll ? m_emittedEntries.values().toVector() : files;

    hid_t root = H5Oopen(file_id, "/", H5P_DEFAULT);

    qDebug("extracting %d files", flist.size());

    // Extract entries.
    m_overwriteAll = false; // Whether to overwrite all files
    m_skipAll = false; // Whether to skip all files

    // Extract all specified entries
    qulonglong i = 0;
    for (const Archive::Entry* e : flist) {
        if (QThread::currentThread()->isInterruptionRequested()) {
            break;
        }
        if (!extractEntry(root, e,
                          destinationDirectory,
                          options)) {
            qCDebug(ARK) << "Extraction failed";
            return false;
        }
        emit progress(float(++i) / flist.size());
    }

    H5Oclose(root);

    return true;
}


#include "hdfplugin.moc"
