/*
 * Copyright (c) 2018 ???
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES ( INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION ) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ( INCLUDING NEGLIGENCE OR OTHERWISE ) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HDFPlugin_H
#define HDFPlugin_H

#include "archiveinterface.h"

#include <QMutex>

#include <hdf5.h> // HDF5
#include <hdf.h> // HDF4

class QFile;

using namespace Kerfuffle;

class HDFPlugin : public ReadOnlyArchiveInterface
{
    Q_OBJECT

public:
    explicit HDFPlugin(QObject *parent, const QVariantList& args);
    ~HDFPlugin() override;

    bool list() override;
    bool doKill() override;
    bool extractFiles(const QVector<Archive::Entry*> &files, const QString& destinationDirectory, const ExtractionOptions& options) override;
    bool testArchive() override;

private:
    void progressEmitted(double pct);
    void listEntry(hid_t g_id, const char *name, const H5L_info_t *info);
    void listAttr(hid_t obj_id, const QString& obj_path);
    void printAttr(QFile& dest, hid_t loc_id, const char* name, const H5A_info_t* info);
    bool extractEntry(hid_t root_id, const Archive::Entry* entry, const QString& destinationDirectory,
                      const ExtractionOptions& options);

    QMap<QString,Archive::Entry*> m_emittedEntries;
    bool m_overwriteAll;
    bool m_skipAll;
    bool m_listAfterAdd;
    QMutex m_mutex;
    hid_t file_id;
    qint64 user_block_size;

    static herr_t iterAttr(hid_t location_id, const char *attr_name, const H5A_info_t *ainfo, void *op_data);
    static herr_t iterLink( hid_t g_id, const char *name, const H5L_info_t *info, void *vplugin);
};

#endif // HDFPlugin_H
