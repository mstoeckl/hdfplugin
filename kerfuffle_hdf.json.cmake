{
    "KPlugin": {
        "Description": "Open and extract the HDF5 archive format",
        "Id": "kerfuffle_hdf",
        "MimeTypes": [
            "@SUPPORTED_MIMETYPES@"
        ],
        "Name": "HDF plugin",
        "ServiceTypes": [
            "Kerfuffle/Plugin"
        ],
        "Version": "@KDE_APPLICATIONS_VERSION@"
    },
    "X-KDE-Kerfuffle-ReadWrite": false,
    "X-KDE-Priority": 100,
    "application/x-hdf": {
    }
}
